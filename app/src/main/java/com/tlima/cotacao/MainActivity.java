package com.tlima.cotacao;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private final String URL_COTACOES = "http://developers.agenciaideias.com.br/cotacoes/json";
    private ProgressDialog dialog;
    private ServiceTask serviceTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dialog = ProgressDialog.show(MainActivity.this, "Download", "Obtendo Cotações");
        serviceTask = new ServiceTask();
        serviceTask.execute(URL_COTACOES);
    }

    @Override
    protected void onDestroy() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

        if (serviceTask != null) {
            serviceTask.cancel(true);
        }
        super.onDestroy();
    }

    protected class ServiceTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                return obterCotacoes(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String json) {
            super.onPostExecute(json);
            dialog.dismiss();
            if (json != null) {
                try {
                    JSONObject jsonObject = new JSONObject(json);

                    JSONObject bovespa = jsonObject.getJSONObject("bovespa");
                    ((TextView) findViewById(R.id.bovespa_cotacao)).setText(bovespa.getString("cotacao"));
                    ((TextView) findViewById(R.id.bovespa_variacao)).setText(bovespa.getString("variacao"));

                    JSONObject dolar = jsonObject.getJSONObject("dolar");
                    ((TextView) findViewById(R.id.dolar_cotacao)).setText(dolar.getString("cotacao"));
                    ((TextView) findViewById(R.id.dolar_variacao)).setText(dolar.getString("variacao"));

                    JSONObject euro = jsonObject.getJSONObject("euro");
                    ((TextView) findViewById(R.id.euro_cotacao)).setText(euro.getString("cotacao"));
                    ((TextView) findViewById(R.id.euro_variacao)).setText(euro.getString("variacao"));

                    ((TextView) findViewById(R.id.data_atualizacao)).setText(jsonObject.getString("atualizacao"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        private String obterCotacoes(String url) throws IOException {

            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setDoInput(true);
            conn.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder sb = new StringBuilder();

            String linha = null;
            while ((linha = reader.readLine()) != null) {
                sb.append(linha + "\n");
            }

            return sb.toString();
        }
    }
}
